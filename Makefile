CC = g++

CFLAGS = -Iinclude -I../src -g -O2 -std=c++11 -Wall -Wextra -Wpedantic -Wno-unused-parameter

SOURCES = \
	src/classifier.cpp \
	src/expression.cpp \
	src/grammar.cpp \
	src/loader.cpp \
	src/node.cpp \
	src/parser.cpp \
	src/router.cpp \
	src/source.cpp \
	src/state.cpp \
	src/token.cpp \
	src/validator.cpp

OBJECTS = $(SOURCES:.cpp=.o)

LIBRARY = libexprail.a

all: $(SOURCES) $(LIBRARY)

$(LIBRARY) : $(OBJECTS)
	ar rcs $(LIBRARY) $(OBJECTS)

.cpp.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm $(OBJECTS)

