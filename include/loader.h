#ifndef LOADER_H
#define LOADER_H

#include "expression.h"

#include <list>
#include <string>

/**
 * Load expressions from a given path
 * @param path the path of the grammar file which contains expressions
 * @return list of expressions
 */
std::list<Expression> loadExpressions(const std::string& path);

#endif // LOADER_H
